const db = uniCloud.database()
exports.main = async (event, context) => {
	return await db.collection("xihu_fruit_notice").orderBy("date", "desc").field({
		info: false
	}).get()
};