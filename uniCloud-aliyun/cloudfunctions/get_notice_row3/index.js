const db = uniCloud.database()
const cmd = db.command
exports.main = async (event, context) => {
	return await db.collection("xihu_fruit_notice").orderBy("date", "desc").limit(3).field({
		info: false
	}).get()
};