const db = uniCloud.database()
const cmd = db.command
exports.main = async (event, context) => {
	return await db.collection("xihu_fruit_notice").doc(event.id).update({
		like: cmd.inc(event.num)
	})
};