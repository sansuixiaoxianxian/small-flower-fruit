const db = uniCloud.database()
exports.main = async (event, context) => {
	return await db.collection("xihu_fruit_notice").doc(event._id).field({
		info: true
	}).get()
};